/* --- colorscheme --- */

color-main : #22273D #394165 #366393

bleu : #29AED7
vert : #00BC9E
orange : #DD7430
rouge : #c43827
violet : #A769BC

/* --- configuration idéale --- */

menu principal    => sous-entête / horizontal déroulant
menu de connexion => entête

