#########################################
##            BIENVENUE                ##
#########################################

-------------------------------- Infos du template -----------------------------


Vous allez installer le th�me : FinalFantasy

-------------------------------- Pour les Background ------------------------------

Les images sont � placer dans le dossier "templates/FinalFantasy/theme/images".
Le fichier CSS � modifier est dans le dossier "templates/FinalFantasy/theme".

Le fond est pr�-r�gl� en css. Vous n'avez qu'a chang� l'image qui devra �tre au format pr�alable/ou sup�rieur � 1280x900.
Attention tout de m�me au poids de votre images, qui risque si elle est lourde de ralentir l'affichage du site.


---------------Pour le background en fixe---------------

Dans le dossier "theme", fichier "Design.css" � la ligne 12 :

background:transparent url(images/bg.jpg) no-repeat scroll top center;	
/*Enlevez ou rajoutez les balises commentaires ci-dessous pour valider ou invalider le css */	
background-attachment:fixed;

---------------Pour le background en top---------------

Dans le dossier "theme", fichier "Design.css" � la ligne 12 :

/* remplacer le -transparent- par une couleur hexad�cimal #XXXXXX, si vous le souhaitez*/
background:transparent url(images/bg.jpg) no-repeat scroll top center;	
/*Enlevez ou rajoutez les balises commentaires ci-dessous pour valider ou invalider le css */	
/*background-attachment:fixed;*/

-------------------------------- Pour finir ------------------------------

N'oublier pas de r�g�n�rer le cache.

----------------------------- Dossier : extras ---------------------------

Vous  trouverez l'image titre, dans le dossier "templates/FinalFantasy/theme/images".

##############################################
##        @micalement Swan : Just For FuN   ##
##############################################